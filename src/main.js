import Vue from "vue";
import App from "./App.vue";
import "@/assets/tailwind.css";
import Chart from "chart.js";
import Chartkick from "vue-chartkick";
import { VueSpinners } from "@saeris/vue-spinners";

import router from "@/router";
import { dollarFilter, percentFilter } from "@/filters";
import vuetify from "./plugins/vuetify";
import VueParticles from "vue-particles";

Vue.use(VueParticles);
Vue.use(VueSpinners);
Vue.use(Chartkick.use(Chart));
Vue.use(
  (Chartkick.options = {
    library: { animation: { easing: "easeOutQuart" } },
  })
);

Vue.filter("dollar", dollarFilter);
Vue.filter("percent", percentFilter);
Vue.config.productionTip = false;

new Vue({
  router,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
