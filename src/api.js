const url = "https://api.coincap.io/v2";

import router from "@/router";

function getAssets() {
  console.log(`${url}/assets?limit=20`);

  return fetch(`${url}/assets?limit=20&`)
    .then((res) => res.json())
    .then((res) => {
      console.log(res.data);
      return res.data;
    })
    .catch((error) => {
      console.log("request failed", error), router.go();
    }); //La Api tiene un limite de 200 consultas, y existen momentos donde se queda congelado el loading, por este error, por tal motivo genero una recarga.
}

function getAsset(coin) {
  return fetch(`${url}/assets/${coin}`)
    .then((res) => res.json())
    .then((res) => res.data)
    .catch((error) => {
      console.log("request failed", error), router.go();
    });
}

function getAssetHistory(coin) {
  const now = new Date();
  const end = now.getTime();
  now.setDate(now.getDate() - 1);
  const start = now.getTime();

  return fetch(
    `${url}/assets/${coin}/history?interval=h1&start=${start}&end=${end}`
  )
    .then((res) => res.json())
    .then((res) => res.data)
    .catch((error) => {
      console.log("request failed", error), router.go();
    });
}

function getMarkets(coin) {
  return fetch(`${url}/assets/${coin}/markets?limit=5`)
    .then((res) => res.json())
    .then((res) => res.data)
    .catch((error) => {
      console.log("request failed", error), router.go();
    });
}

function getExchange(id) {
  return fetch(`${url}/exchanges/${id}`)
    .then((res) => res.json())
    .then((res) => res.data)
    .catch((error) => {
      console.log("request failed", error), router.go();
    });
}

export default {
  getAssets,
  getAsset,
  getMarkets,
  getExchange,
  getAssetHistory,
};
