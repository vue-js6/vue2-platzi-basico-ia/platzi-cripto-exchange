# Proyecto del Curso Basico de Vue.js2

## Platzi Cripto Exchange

Instruido por:

Ignacio Anaya

Descripción:

Platzi Exchange es una plataforma que te servirá para visualizar todas las criptomonedas y sus valores en tiempo real utilizando las ventajas de Vue.js un framework para desarrollar sitios web reactivos y basados en componentes muy utilizado en el mundo del desarrollo web

🚀 Link para ver el proyecto: (https://cripto-exchange-jv.netlify.app/)

(https://www.youtube.com/watch?v=8q7mAVVwNgE)

[![PlatziCriptoExchange](https://i.imgur.com/UnACB8g.png)](https://www.youtube.com/watch?v=8q7mAVVwNgE)
[![PlatziCriptoExchange](https://i.imgur.com/JFfurEm.png)](https://www.youtube.com/watch?v=8q7mAVVwNgE)
[![PlatziCriptoExchange](https://i.imgur.com/kN3tXin.png)](https://www.youtube.com/watch?v=8q7mAVVwNgE)
[![PlatziCriptoExchange](https://i.imgur.com/I3siHaR.png)](https://www.youtube.com/watch?v=8q7mAVVwNgE)

## Nota 🏁 (Consiguraciones):

inicializamos:

`vue create nombreproyecto`

Api:
[ApiCoincap](https://docs.coincap.io/)

### Comandos:

- `npm install vue-router`
- `npm i @saeris/vue-spinners`
- `vue add tailwind` seleccionar full
- `npm install numeral`
- `npm install chart.js@2.9.4`
- `npm i vue-chartkick@0.6.0`
- `vue add vuetify`
- `npm install vue-particles --save-dev`

Dato: `npm i vue-chartkick` la ultima version de chartkick es solo compatible con Vue3, por eso se uso la vue-chartkick@0.6.0 que si es compatible con Vue2, esto pasa lo mismo con chart.js

## Despliegue netlify (vinculando con Gitlab)🚩:

Link para ver el proyecto: (https://cripto-exchange-jv.netlify.app/)

Luego del proceso de vincular con Gital, debemos colocar esto en el paso 3 del despliegue:

- Build command `npm run build`
- Publish directory `dist`

Se desplegara al instante, pero si tienes problemas como yo, que carga la primera vista, pero no carga otras vista por ejemplo `/about /coin/:id o la vista de error` y te aparece un modal con un mensaje de: Page Not Found, entonces:

- Crea un archivo \_redirects dentro de /public, y colocas lo siguiente dentro.

```
/* /index.html 200
```

#### Esta documentación me ayudo:

- (https://medium.com/@ishoshot/page-not-found-on-reload-vuejs-netlify-c71716e97e6)

## :+1: Sígueme en mis redes sociales:

- WebPersonal: (https://jorge-vicuna.gitlab.io/jorge-vicuna/)
- GitLab: (https://gitlab.com/jorge_vicuna)
- Youtube: (https://www.youtube.com/channel/UCW0m1TKKiN3Etejqx6h3Jtg)
- Linkedin: (https://www.linkedin.com/in/jorge-vicuna-valle/)
- Facebook: (https://www.facebook.com/jorge.vicunavalle/)
